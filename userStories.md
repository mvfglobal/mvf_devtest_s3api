# User Stories / Epics

When choosing from this list, please consider value and quality over quantity, and showcase your software engineering skills.

## Epic 1: Expose static data from a single file held locally for customer a4a06bb0-3fbe-40bd-9db2-f68354ba742f
Download https://mvf-devtest-s3api.s3-eu-west-1.amazonaws.com/a4a06bb0-3fbe-40bd-9db2-f68354ba742f.json and expose data within your app.
Except for the data requirement, this epic has stories identical to Epic 2.

> **1.1**
> As an account holder,
> I want to check my balance,
> so that I know how much money I have.

> **1.2**
> As an account holder,
> I want to check the details being held about me,
> so that I can make sure the correct details are being stored.

> * Outputs: First name, Last name, Email, Telephone

> **1.3**
> As the customer,
> I want to get a list of accounts in debt,
> so that I can assess them for overdraft interest.

> * Outputs: account guid

> **1.4**
> As the customer,
> I want to get the name, email address, telephone and balance for an account,
> so that I can contact them and talk about their account.

## Epic 2: Expose dynamic data from our S3 bucket for all customers
Serve data dynamically from https://mvf-devtest-s3api.s3-eu-west-1.amazonaws.com/ for all customers in the bucket.
Except for the data requirement, this epic has stories identical to Epic 1.

> **2.1**
> As an account holder,
> I want to check my balance,
> so that I know how much money I have.

> **2.2**
> As an account holder,
> I want to check the details being held about me,
> so that I can make sure the correct details are being stored.

> * Outputs: First name, Last name, Email, Telephone

> **2.3**
> As a customer,
> I want to get a list of accounts in debt,
> so that I can assess them for overdraft interest.

> * Outputs: account guid

> **2.4**
> As a customer,
> I want to get the name, email address, telephone and balance for an account,
> so that I can contact them and talk about their account.

## Epic 3: Bonus Stories

> **3.1**
> As a customer,
> I want to search for accounts by firstname and/or lastname,
> so that I can find the right customer when they contact me

> * Inputs: firstname and/or lastname
 
> * Outputs: account guids

> **3.2**
> As a customer,
> I want to search for accounts by firstname and/or lastname, even when it's only partial or mis-spelled,
> so that I can quickly find the right customer when they contact me, even if I misheard their name.

> * Inputs: firstname and/or lastname

> * Outputs: account guids ordered by confidence metric

> **3.3**
> As a customer,
> I want to get a list of accounts with balances in certain limits,
> so that I can offer different products and services to savers and borrowers.

> * Inputs: minimum balance and/or maximum balance

> * Outputs: account guids ordered by balance

